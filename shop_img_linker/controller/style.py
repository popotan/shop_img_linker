from flask import Blueprint, render_template, jsonify, request

from sqlalchemy import desc, asc, text

from shop_img_linker.model.match_info import MatchInfoORM
from shop_img_linker.model.image_info import ImageInfoORM
from shop_img_linker.model.prod_info import ProdInfoORM

from shop_img_linker.composer.StyleMaker import StyleMaker
style = Blueprint('style', __name__)

@style.route('/<int:prod_id>/<target_shop>', methods=['GET'])
def mk_style(prod_id, target_shop):
	match_info_list = MatchInfoORM().query\
		.filter_by(prod_id=prod_id)\
		.filter(MatchInfoORM._shop.ilike(text('"%' + target_shop + '%"')))\
		.order_by(asc(MatchInfoORM.seq))\
		.all()

	html = ['<p style="text-align:center">']
	for obj in match_info_list:
		html.append(StyleMaker(target_shop, obj, 'img').RESULT)
	html.append('</p>')
	result = '\r\n'.join(html)
	return result
