from flask import Blueprint, render_template, jsonify, request

from shop_img_linker.model.prod_info import ProdInfoORM
from shop_img_linker.model.match_info import MatchInfoORM
product = Blueprint('product', __name__)

@product.route('', methods=['GET', 'POST'])
def index():
	if request.method == 'GET':
		product_list = ProdInfoORM().query.all()
		repr_cols = ['prod_id', 'prod_name', 'reg_date']
		result = [{_col : getattr(_row, _col) for _col in repr_cols} for _row in product_list]
		return jsonify(result)
	elif request.method == 'POST':
		args = request.get_json()
		product = ProdInfoORM()
		product.prod_name = args['prod_name']
		product.add()

		repr_cols = ['prod_id', 'prod_name', 'reg_date']
		result = {_col : getattr(product, _col) for _col in repr_cols}
		return jsonify(result)

@product.route('/<int:prod_id>', methods=['GET', 'POST', 'DELETE'])
def product_info(prod_id):
	if request.method == 'GET':
		product_info = ProdInfoORM().query.filter_by(prod_id=prod_id).first()
		repr_cols = ['prod_id', 'prod_name', 'reg_date']
		result = {_col : getattr(product_info, _col) for _col in repr_cols}
		return jsonify(result)
	elif request.method == 'POST':
		args = request.get_json()
		product = ProdInfoORM().query.filter_by(prod_id=prod_id).first()
		product.prod_name = args['prod_name']
		product.update()

		repr_cols = ['prod_id', 'prod_name', 'reg_date']
		result = {_col : getattr(product, _col) for _col in repr_cols}
		return jsonify(result)
	elif request.method == 'DELETE':
		product = ProdInfoORM().query.filter_by(prod_id=prod_id).first()
		product.delete()

		match_info = MatchInfoORM().query.filter_by(prod_id=prod_id).all()
		for row in match_info:
			row.delete()
		return ('', 201)