from flask import Blueprint, render_template, jsonify, request

from sqlalchemy import desc, asc

from shop_img_linker.model.match_info import MatchInfoORM
from shop_img_linker.model.image_info import ImageInfoORM
from shop_img_linker.model.prod_info import ProdInfoORM
match = Blueprint('match', __name__)

@match.route('/<int:match_id>', methods=['GET', 'POST'])
def match_info(match_id):
	if request.method == 'GET':
		return ('', 200)
	elif request.method == 'POST':
		args = request.get_json()
		match_info = MatchInfoORM().query.filter_by(match_id=match_id).first()
		match_info.image_id = args['image_id']
		match_info.prod_id = args['prod_id']
		match_info.seq = args['seq']
		match_info.shop = args['shop']
		match_info.update()

		repr_cols = ['match_id', 'image_id', 'prod_id', 'seq', 'match_tag', 'shop', 'reg_date', 'image_info', 'prod_info']
		result = {_col : getattr(match_info, _col) for _col in repr_cols}
		return jsonify(result)

@match.route('/product/<int:prod_id>', methods=['GET'])
def get_relative_image_list(prod_id):
	match_info = MatchInfoORM().query.filter_by(prod_id=prod_id).order_by(asc(MatchInfoORM.seq)).all()
	if match_info:
		repr_cols = ['match_id', 'image_id', 'prod_id', 'seq', 'match_tag', 'shop', 'reg_date', 'image_info', 'prod_info']
		result = [{_col : getattr(_row, _col) for _col in repr_cols} for _row in match_info]
	else:
		result = []
	return jsonify(result)

@match.route('/image/<int:image_id>', methods=['GET'])
def get_relative_prod_list(image_id):
	match_info = MatchInfoORM().query.filter_by(image_id=image_id).all()
	if match_info:
		repr_cols = ['match_id', 'image_id', 'prod_id', 'seq', 'match_tag', 'shop', 'reg_date', 'image_info', 'prod_info']
		result = [{_col : getattr(_row, _col) for _col in repr_cols} for _row in match_info]
	else:
		result = []
	return jsonify(result)

def set_seq(prod_id):
	last_seq_match = MatchInfoORM().query.filter_by(prod_id=prod_id).order_by(desc(MatchInfoORM.seq)).first()
	if last_seq_match:
		if last_seq_match.seq > 0:
			result = last_seq_match.seq + 1
		else: result = 1
	else: result = 1
	return result

@match.route('/image/with/<int:prod_id>', methods=['POST'])
def match_image_with_prod(prod_id):
	value = request.get_json()
	match_info = MatchInfoORM()
	match_info.prod_id = prod_id
	match_info.image_id = value['image_id']
	match_info.shop = ['storefarm', 'firstmall', '11st', 'auction', 'interpark']
	match_info.seq = set_seq(prod_id)
	match_info.add()
	return ('', 200)

@match.route('/seq/switch/<int:pTarget>/to/<int:iTarget>', methods=['POST'])
def switch_seq(pTarget, iTarget):
	pTarget_info = MatchInfoORM().query.filter_by(match_id=pTarget).first()
	iTarget_info = MatchInfoORM().query.filter_by(match_id=iTarget).first()
	(pTarget_info.seq, iTarget_info.seq) = (iTarget_info.seq, pTarget_info.seq)
	pTarget_info.update()
	iTarget_info.update()

	repr_cols = ['match_id', 'image_id', 'prod_id', 'seq', 'match_tag', 'shop', 'reg_date', 'image_info', 'prod_info']
	pResult = {_col : getattr(pTarget_info, _col) for _col in repr_cols}
	iResult = {_col : getattr(iTarget_info, _col) for _col in repr_cols}
	return jsonify({'pResult' : pResult, 'iResult' : iResult})

@match.route('/unlink/<int:match_id>', methods=['DELETE'])
def unlink_match_info(match_id):
	if request.method == 'DELETE':
		match_info = MatchInfoORM().query.filter_by(match_id=match_id).first()
		match_info.delete()
		return ('', 201)
	else:
		return ('', 404)

@match.route('/tag/result', methods=['GET', 'POST'])
def make_tag():
	if request.method == 'GET':
		pass
	elif request.method == 'POST':
		pass