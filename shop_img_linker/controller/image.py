from flask import Blueprint, render_template, jsonify, request

from sqlalchemy import or_, text, asc

from shop_img_linker.model.image_info import ImageInfoORM
from shop_img_linker.model.match_info import MatchInfoORM

image = Blueprint('image', __name__)

@image.route('', methods=['GET', 'POST'])
def index():
	if request.method == 'GET':
		image_list = ImageInfoORM().query.all()
		repr_cols = ['image_id', 'image_tag', 'image_filename', 'division', 'reg_date']
		result = [{_col : getattr(_row, _col) for _col in repr_cols} for _row in image_list]
		return jsonify(result)
	elif request.method == 'POST':
		args = request.get_json()
		image = ImageInfoORM()
		image.image_tag = args['image_tag']
		image.division = 'content'
		image.add()

		repr_cols = ['image_id', 'image_tag', 'href', 'image_filename', 'division', 'reg_date']
		result = {_col : getattr(image, _col) for _col in repr_cols}
		return jsonify(result)

@image.route('/<int:image_id>', methods=['GET', 'POST', 'DELETE'])
def image_info(image_id):
	if request.method == 'GET':
		image_info = ImageInfoORM().query.filter_by(image_id=image_id).first()
		repr_cols = ['image_id', 'image_tag', 'href', 'image_filename', 'division', 'reg_date']
		result = {_col : getattr(image_info, _col) for _col in repr_cols}
		return jsonify(result)
	elif request.method == 'POST':
		args = request.get_json()
		image = ImageInfoORM().query.filter_by(image_id=image_id).first()
		image.image_tag = args['image_tag']
		image.division = args['division']
		image.href = args['href']
		image.update()

		repr_cols = ['image_id', 'image_tag', 'href', 'image_filename', 'division', 'reg_date']
		result = {_col : getattr(image, _col) for _col in repr_cols}
		return jsonify(result)
	elif request.method == 'DELETE':
		image = ImageInfoORM().query.filter_by(image_id=image_id).first()
		image.delete()

		match_info = MatchInfoORM().query.filter_by(image_id=image_id).all()
		for row in match_info:
			row.delete()

		return ('', 201)

@image.route('/search', methods=['GET'])
def search_image():
	word = request.args.get('keyword')
	image_list = ImageInfoORM().query.filter(ImageInfoORM.image_tag.ilike(text('"%' + word + '%"')))
	image_list = image_list.all()
	repr_cols = ['image_id', 'image_tag', 'href', 'image_filename', 'division', 'reg_date']
	result = [{_col : getattr(_row, _col) for _col in repr_cols} for _row in image_list]
	return jsonify(result)

from shop_img_linker.composer.ImageUploader import ImageUploader
@image.route('/upload/<int:image_id>', methods=['POST'])
def upload_file(image_id):
	if request.files['image_file'] != '':
		image_info = ImageInfoORM().query.filter_by(image_id=image_id).first()
		image_info.image_filename = ImageUploader(target_file=request.files['image_file'],target_name=str(image_info.image_id)).FILENAME
		image_info.update()

		repr_cols = ['image_id', 'image_tag', 'href', 'image_filename', 'division', 'reg_date']
		result = {_col : getattr(image_info, _col) for _col in repr_cols}
		return jsonify(result)
	else:
		return ('', 500)
	