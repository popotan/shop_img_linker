from flask import Blueprint, render_template

crop = Blueprint('crop', __name__)

@crop.route('/')
def index():
	return render_template('index.html')