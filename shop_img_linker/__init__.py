from flask import Flask, send_from_directory, session, request, render_template, g
from flask_session import Session
from flask_bcrypt import Bcrypt

from shop_img_linker.config import database
from datetime import timedelta

app = Flask(__name__, template_folder='template', static_url_path='/static')


# from werkzeug.serving import run_simple
# from werkzeug.wsgi import DispatcherMiddleware
# def simple(env, resp):
#     resp(b'200 OK', [(b'Content-Type', b'text/plain')])
#     return [b'Hello WSGI World']
# app.wsgi_app = DispatcherMiddleware(simple, {'/host': app.wsgi_app})

# app.config["APPLICATION_ROOT"] = "/host"
app.secret_key = 'funclass-shop-img-linker'

app.config.from_object(database)
from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()
db.init_app(app)

@app.route('/static/<path:path>')
def send_js(path):
	"""
		static 파일들을 템플릿상에서 url_for 없이 사용하기 위해 라우팅 제공
	"""
	path = change_name_tail(path)
	return send_from_directory('static/', path)

def change_name_tail(path):
	allow_tail = ['jpg', 'jpeg', 'png', 'gif']
	dir_path = path.split('/')[:-1]
	filename = path.split('/')[-1]
	name_and_tail = filename.split('.')

	if name_and_tail[-1] in allow_tail: #그림파일이면
		for t in allow_tail:
			test_name = '/'.join(dir_path + filename) + '.' + t
			if os.path.exists(test_name):
				return test_name
	return path

from shop_img_linker.controller.admin import admin
from shop_img_linker.controller.image import image
from shop_img_linker.controller.crop import crop
from shop_img_linker.controller.match import match
from shop_img_linker.controller.product import product
from shop_img_linker.controller.style import style

app.register_blueprint(admin, url_prefix='')
app.register_blueprint(image, url_prefix='/api/image')
app.register_blueprint(crop, url_prefix='/api/image/crop')
app.register_blueprint(product, url_prefix='/api/product')
app.register_blueprint(match, url_prefix='/api/match')
app.register_blueprint(style, url_prefix='/api/style')