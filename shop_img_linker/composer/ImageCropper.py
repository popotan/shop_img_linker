from PIL import Image
import os

class ImageCropper(object):
	"""docstring for Image_cropper"""
	
	#설정정보
	IMAGE_FILE_PATH = ''
	SAVE_PATH = ''
	IMAGE_SIZES = []
	IMAGE_BACKGROUND_COLOR = ''

	def __init__(self, image_path):
		super(Image_cropper, self).__init__()
		self.IMAGE_FILE_PATH = image_path

	def open_file(self):
		self.ORIGIN_IMAGE = Image.open(self.IMAGE_FILE_PATH)
		#이미지 정보들
		self.ORIGIN_WIDTH, self.ORIGIN_HEIGHT = self.ORIGIN_IMAGE.size
		self.ORIGIN_FILENAME = self.IMAGE_FILE_PATH.split('/')[-1]
		self.ORIGIN_FORMAT = self.ORIGIN_IMAGE.format

		for shoppingmall_name in self.IMAGE_SIZES:
			for size in self.IMAGE_SIZES[shoppingmall_name]:
				self.crop_image(	shoppingmall_name=shoppingmall_name,
									image_obj=self.ORIGIN_IMAGE,
									to_width=size[0],
									to_height=size[1]	)

	def crop_image(self, image_obj, shoppingmall_name, to_width, to_height):
		if to_width == to_height and self.ORIGIN_WIDTH == self.ORIGIN_HEIGHT:
			result_obj = image_obj.resize((to_width, to_height))
		else:
			result_obj = self.get_position_of_crop_target(image_obj, to_width, to_height)
		self.save_image(shoppingmall_name=shoppingmall_name, image_obj=result_obj)

	def get_position_of_crop_target(self, image_obj, to_width, to_height):
		#큰쪽 사이즈 알아내기(기준축 설정)
		x_asis, y_asis = 0, 0
		left, top, right, bottom = 0, 0, 0, 0
		
		if self.IMAGE_BACKGROUND_COLOR == 'white':
			new_image_obj = Image.new(mode='RGBA',size=(to_width, to_height),color=(255,255,255,1000)) #바탕
		elif self.IMAGE_BACKGROUND_COLOR == 'black':
			new_image_obj = Image.new(mode='RGBA',size=(to_width, to_height),color=(0,0,0,1000)) #바탕
		else:
			new_image_obj = Image.new(mode='RGBA',size=(to_width, to_height),color=(255,255,255,1000)) #바탕

		if self.ORIGIN_WIDTH != self.ORIGIN_HEIGHT:
			image_obj = self.make_image_ract(image_obj)

		if to_width > to_height:
			#이미지를 작은쪽 사이즈에 리사이징하기
			image_obj = image_obj.resize((to_height, to_height))
			#y축방향
			y_asis = int(abs(to_width - to_height) / 2)
			# top = -1 * y_asis
			# bottom = top + to_height
			# right = to_width
			new_image_obj.paste(image_obj, (y_asis, 0))
			return new_image_obj
		else:
			#이미지를 작은쪽 사이즈에 리사이징하기
			image_obj = image_obj.resize((to_width, to_width))
			#x축방향
			x_asis = int(abs(to_width - to_height) / 2)
			# left = -1 * x_asis
			# right = left + to_width
			# bottom = to_height
			new_image_obj.paste(image_obj, (0, x_asis))
			return new_image_obj

	def make_image_ract(self, image_obj):
		if self.ORIGIN_WIDTH > self.ORIGIN_HEIGHT:
			if self.IMAGE_BACKGROUND_COLOR == 'white':
				new_image_obj = Image.new(mode='RGBA',size=(self.ORIGIN_WIDTH, self.ORIGIN_WIDTH),color=(255,255,255,1000)) #바탕
			elif self.IMAGE_BACKGROUND_COLOR == 'black':
				new_image_obj = Image.new(mode='RGBA',size=(self.ORIGIN_WIDTH, self.ORIGIN_WIDTH),color=(0,0,0,1000)) #바탕
			else:
				new_image_obj = Image.new(mode='RGBA',size=(self.ORIGIN_WIDTH, self.ORIGIN_WIDTH),color=(255,255,255,1000)) #바탕

			y_asis = int(abs(self.ORIGIN_WIDTH - self.ORIGIN_HEIGHT) / 2)
			new_image_obj.paste(image_obj, (0, y_asis))
		else:
			if self.IMAGE_BACKGROUND_COLOR == 'white':
				new_image_obj = Image.new(mode='RGBA',size=(self.ORIGIN_HEIGHT, self.ORIGIN_HEIGHT),color=(255,255,255,1000)) #바탕
			elif self.IMAGE_BACKGROUND_COLOR == 'black':
				new_image_obj = Image.new(mode='RGBA',size=(self.ORIGIN_HEIGHT, self.ORIGIN_HEIGHT),color=(0,0,0,1000)) #바탕
			else:
				new_image_obj = Image.new(mode='RGBA',size=(self.ORIGIN_HEIGHT, self.ORIGIN_HEIGHT),color=(255,255,255,1000)) #바탕

			x_asis = int(abs(self.ORIGIN_WIDTH - self.ORIGIN_HEIGHT) / 2)
			new_image_obj.paste(image_obj, (x_asis, 0))
		return new_image_obj

	def save_image(self, shoppingmall_name, image_obj):
		fwidth, fheight = image_obj.size
		final_filename = ''.join(self.ORIGIN_FILENAME.split('.')[:-1])
		final_filename = self.make_allow_filename(final_filename)

		image_obj.save(self.SAVE_PATH + '/' + shoppingmall_name + ' ' + str(fwidth) + '-' + str(fheight) + ' ' + final_filename + '.' + self.ORIGIN_FILENAME.split('.')[-1], self.ORIGIN_FORMAT, quality=100)

	def make_allow_filename(self, filename):
		deallow_chars = ['\\','/',':','*','?','"','<','>','|','[',']','{','}','(',')','!',';','.',"'",'#','$','%','\t', '\n', '\r']
		splited_filename = filename.split()
		for char in splited_filename:
			if char in deallow_chars:
				char = '_'
		return ''.join(splited_filename)
