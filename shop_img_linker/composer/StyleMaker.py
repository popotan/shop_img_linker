from shop_img_linker.composer.HtmlParser import HtmlParser
class StyleMaker(object):
	"""docstring for StyleMaker"""
	def __init__(self, shop, target, element_tag):
		self.SHOP = shop
		self.TARGET_OBJ = target
		self.ELEMENT_TAG = element_tag
		self.RESULT = ''

		self.set_element()
		self.set_attribute()
		self.mk_result()

	def set_element(self):
		html_obj = {'tag' : self.ELEMENT_TAG}
		self.HTML_OBJ = HtmlParser(html_obj)

	def set_attribute(self):
		self.HTML_OBJ.TARGET_HTML['attr'] = {
		'style' : 'max-width:100%;min-width:51%;',
		'src' : 'http://host.soundpick.net/static/image/' + self.TARGET_OBJ.image_info['image_filename']
		}

	def set_anchor(self):
		html = '<a target="_blank" href="' + self.TARGET_OBJ.image_info['href'] + '">'
		html += self.RESULT
		html += '</a>'
		self.RESULT = html

	def mk_result(self):
		if self.TARGET_OBJ.image_info['href']:
			self.RESULT = self.HTML_OBJ.html_object_to_string()
			self.set_anchor()
		else:
			self.RESULT = self.HTML_OBJ.html_object_to_string()