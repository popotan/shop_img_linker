from werkzeug import secure_filename
import os, sys
from shutil import copyfile

class ImageUploader(object):
	"""docstring for ImageUploader"""
	ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif', 'JPG', 'PNG', 'JPEG', 'GIF'])
	def __init__(self, target_file, target_name):
		self.TARGET_FILE = target_file
		self.TARGET_NAME = target_name
		self.image_upload()

	def image_upload(self):
		file = self.TARGET_FILE
		# if file and allowed_file(file.filename):
			# filename = secure_filename(file.filename)
		target_path = '/mnt/www/host.soundpick.net/shop_img_linker/static/image/' +self.TARGET_NAME + '.'
		for t in self.ALLOWED_EXTENSIONS:
			if os.path.exists(target_path + t):
				os.remove(target_path + t)
		
		if self.TARGET_FILE.filename.rsplit('.', 1)[1] == 'jpg' or self.TARGET_FILE.filename.rsplit('.', 1)[1] == 'jpeg':
			file.save(os.path.join('/mnt/www/host.soundpick.net/shop_img_linker/static/image', self.TARGET_NAME + '.' + 'jpg'))
			copyfile(os.path.join('/mnt/www/host.soundpick.net/shop_img_linker/static/image', self.TARGET_NAME + '.' + 'jpg'), 
				os.path.join('/mnt/www/host.soundpick.net/shop_img_linker/static/image', self.TARGET_NAME + '.' + 'jpeg'))
		else:
			file.save(os.path.join('/mnt/www/host.soundpick.net/shop_img_linker/static/image', 
				self.TARGET_NAME + '.' + self.TARGET_FILE.filename.rsplit('.', 1)[1]))
		# file.save(os.path.join('/mnt/www/host.soundpick.net/shop_img_linker/static/image', 
		# 	self.TARGET_NAME + '.' + tails[1]))
		# file.save(os.path.join('/mnt/www/host.soundpick.net/shop_img_linker/static/image', 
		# 	self.TARGET_NAME + '.' + tails[2]))
		# file.save(os.path.join('/mnt/www/host.soundpick.net/shop_img_linker/static/image', 
		# 	self.TARGET_NAME + '.' + tails[3]))

		self.FILENAME = self.TARGET_NAME + '.' + self.TARGET_FILE.filename.rsplit('.', 1)[1]
		# else:
		# 	return ('', 500)

	def allowed_file(self, filename):
		return '.' in filename and \
			filename.rsplit('.', 1)[1] in self.ALLOWED_EXTENSIONS