import re
class HtmlParser(object):
	"""docstring for StringParser"""
	def __init__(self, target):
		if isinstance(target, str):
			self.init_global_field('TARGET_STRING')
			self.TARGET_STRING = target
		elif isinstance(target, dict):
			self.init_global_field('TARGET_HTML')
			self.TARGET_HTML = target

	def init_global_field(self, target):
		if target == 'TARGET_STRING':
			self.TARGET_STRING = ''
		elif target == 'TARGET_HTML':
			self.TARGET_HTML = {
				'tag' : '',
				'attr' : {}
			}

	def check_is_html_string(self):
		if re.match("<img[ ].*>", self.TARGET_STRING):
			return True
		elif re.match("<img[ ].*/>", self.TARGET_STRING):
			return True
		else:
			return False

	def string_to_html_object(self):
		self.init_global_field('TARGET_HTML')
		if self.check_is_html_string():
			self.TARGET_HTML['tag'] = self.TARGET_STRING.split(' ')[0].replace('<')
			splited = self.TARGET_STRING.split(' ')[1:]
			if splited[-1] == '/>' or splited[-1] == '>':
				splited.pop()
			if splited[-1].endswith('/>'):
				splited[-1].replace('/>', '')
			elif splited[-1].endswith('>'):
				splited[-1].replace('>', '')

			for attr_keyval in splited:
				keyval = attr_keyval.split('=')
				attr_name = keyval[0]
				attr_value = keyval[1]
				attr_arr.setDefault(attr_name, attr_value)
				self.TARGET_HTML['attr'][attr_name] = re.sub(r"(\'|\")(.*)(\'|\")", '\g<2>', attr_value)
			return self.TARGET_HTML
		else:
			return self.TARGET_HTML

	def html_object_to_string(self):
		self.init_global_field('TARGET_STRING')
		fstr = "<" + self.TARGET_HTML['tag'] + " "
		for attr_name in self.TARGET_HTML['attr']:
			fstr += attr_name + '=' + '"' + self.TARGET_HTML['attr'][attr_name] + '" '
		fstr += '/>'
		self.TARGET_STRING = fstr
		return self.TARGET_STRING