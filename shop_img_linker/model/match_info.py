# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime
from flask_sqlalchemy import SQLAlchemy

from shop_img_linker import db
from shop_img_linker.model.prod_info import ProdInfoORM
from shop_img_linker.model.image_info import ImageInfoORM

from datetime import datetime, timedelta

class MatchInfoORM(db.Model):
    __tablename__ = 'match_info'
    match_id = Column(Integer, primary_key=True, unique=True)
    image_id = Column(Integer)
    prod_id = Column(Integer)
    seq = Column(Integer)
    match_tag = Column(String(50))
    _shop = Column('shop', String(100))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    _image_info = {}
    _prod_info = {}

    @property
    def shop(self):
        if self._shop:
            return self._shop.split(',')
        else:
            return []

    @shop.setter
    def shop(self, shop):
        if type(shop) == type(list()):
            self._shop = ','.join(shop)
        else:
            self._shop = shop

    @property
    def image_info(self):
        if self.image_id:
            image = ImageInfoORM().query.filter_by(image_id=self.image_id).first()
            repr_cols = ['image_id', 'image_tag', 'href', 'image_filename', 'division', 'reg_date']
            self._image_info = {_col : getattr(image, _col) for _col in repr_cols}
        return self._image_info

    @property
    def prod_info(self):
        if self.prod_id:
            prod = ProdInfoORM().query.filter_by(prod_id=self.prod_id).first()
            repr_cols = ['prod_id', 'prod_name', 'reg_date']
            self._prod_info = {_col : getattr(prod, _col) for _col in repr_cols}
        return self._prod_info

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()