# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime
from flask_sqlalchemy import SQLAlchemy

from shop_img_linker import db

from datetime import datetime, timedelta

class ImageInfoORM(db.Model):
    __tablename__ = 'image_info'
    image_id = Column(Integer,primary_key=True,unique=True)
    image_tag = Column(String(100))
    image_filename = Column(String(300))
    href = Column(String(1000))
    division = Column(String(45))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()