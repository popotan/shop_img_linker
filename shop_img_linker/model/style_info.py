# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime
from flask_sqlalchemy import SQLAlchemy

from shop_img_linker import db

from datetime import datetime, timedelta

class StyleInfoORM(db.Model):
    __tablename__ = 'style_info'
    style_id = Column(Integer, primary_key=True, unique=True)
    match_id = Column(Integer)
    css = Column(String(1000))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()