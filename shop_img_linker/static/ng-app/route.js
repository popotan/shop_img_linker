"use strict";
angular.module("shop-img-linker")
.config(function($routeProvider, $provide, $httpProvider, $locationProvider){
	$routeProvider
	.when('/', {
		redirectTo: '/image'
	})
	.when('/image', {
		templateUrl : '/static/ng-app/image/template/imageTemplate.html',
		controller : 'imageController',
		controllerAs : 'imageCtrl',
		reloadOnSearch : false
	})
	.when('/product', {
		templateUrl : '/static/ng-app/product/template/productTemplate.html',
		controller : 'productController',
		controllerAs : 'productCtrl',
		reloadOnSearch : false
	})
	.otherwise({redirectTo: '/'});
	$locationProvider.html5Mode(false).hashPrefix('');
});