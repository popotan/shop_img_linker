"use strict";
angular.module('shop-img-linker', ['ngRoute', 'ngResource', 'ngAnimate', 'ngCookies', 'ngFileUpload'])
.config(function($httpProvider){
	$httpProvider.useApplyAsync(true);
})
.run(function($rootScope, $http, $window, $location, labelService){
	$rootScope.current_path = $location.path();

	$rootScope.translate = labelService.translate;
})
.service('labelService', function(){
	var service = this;

	service.dict = {
		'storefarm' : '스토어팜',
		'firstmall' : '퍼스트몰',
		'auction' : '옥션/지마켓',
		'11st' : '11번가',
		'interpark' : '인터파크',
		'content' : '본문',
		'stitle' : '섹션타이틀',
		'prod_desc' : '상품설명(제공)'
	};

	service.translate = function(key){
		if (typeof service.dict[key] == 'undefined') {
			return '';
		}else{
			return service.dict[key];
		}
	}
});