angular.module('shop-img-linker')
.controller('imageController', function($scope, $http, $routeParams, $location){
	var imageCtrl = this;

	imageCtrl.get_image_list = function(){
		$http.get('/api/image').then(function(res){
			if (res.status == 200) {
				$scope.image_list = res.data;
			}
		});
	};
	imageCtrl.get_image_list();

	$scope.$on('image_list:remove', function(event, data){
		angular.forEach($scope.image_list, function(item, idx){
			if (item.image_id == data.image_id) {
				$scope.image_list.splice(idx, 1);
				$location.url($location.path());
				return;
			}
		});
	});

	$scope.$on('image_list:modify', function(event, data){
		angular.forEach($scope.image_list, function(item, idx){
			if (item.image_id == data.image_id) {
				$scope.image_list[idx] = data;
				return;
			}
		});
	});
});