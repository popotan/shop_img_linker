angular.module('shop-img-linker')
.controller('imageListController', function($scope, $http, $location, $element, $routeParams){
	var imageListCtrl = this;

	imageListCtrl.open_info = function(image_id){
		$location.search('id', image_id);
	}

	$scope.keyword = { image_tag : '' };

	imageListCtrl.image_info_init = {
		image_tag : ''
	};

	imageListCtrl.add_image_list = function(){
		if (imageListCtrl.image_info_init.image_tag != '') {
			$http.post('/api/image', imageListCtrl.image_info_init).then(function(res){
				if (res.status == 200) {
					$scope.image_list.push(res.data);
					imageListCtrl.open_info(res.data.image_id);
					imageListCtrl.image_info_init.image_tag = '';
				}
			});
		}else{
			alert('이미지명을 입력해주세요.');
		}
	}
});