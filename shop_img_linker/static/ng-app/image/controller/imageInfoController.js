angular.module('shop-img-linker')
.controller('imageInfoController', function($scope, $http, $location, Upload){
	var imageInfoCtrl = this;

	$scope.$watch(function(){
		return parseInt($location.search().id);
	}, function(newval, oldval){
		if (isNaN(newval)) return;
		// if (newval===oldval)return;
		imageInfoCtrl.init();
		imageInfoCtrl.get_image_info();
	}, true);

	imageInfoCtrl.init = function(){
		imageInfoCtrl.image_info = {};
		imageInfoCtrl.match_info = [];
	}

	imageInfoCtrl.get_image_info = function(){
		if ($location.search().id > 0) {
			$http.get('/api/image/' + parseInt($location.search().id)).then(function(res){
				if (res.status == 200) {
					imageInfoCtrl.image_info = res.data;
					imageInfoCtrl.get_relative_prod_info();
				}
			});
		}
	}

	imageInfoCtrl.mod_image_info = function(){
		$http.post('/api/image/' + parseInt($location.search().id), imageInfoCtrl.image_info).then(function(res){
			if (res.status == 200) {
				imageInfoCtrl.image_info = res.data;
				alert("변경내용이 반영되었습니다.");
				$scope.$emit('image_list:modify', res.data);
			}
		});
	}

	imageInfoCtrl.remove_image_info = function(){
		if(confirm('해당 이미지를 삭제하시겠습니까?\r\n(상품정보에 연동된 이미지도 함께 삭제됩니다)')){
			$http.delete('/api/image/' + parseInt($location.search().id)).then(function(res){
				if (res.status == 201) {
					alert('삭제되었습니다.');
					imageInfoCtrl.image_info = undefined;
					$scope.$emit('image_list:remove', {image_id:parseInt($location.search().id)});
				}
			});
		}
	}

	imageInfoCtrl.get_relative_prod_info = function(){
		$http.get('/api/match/image/' + parseInt($location.search().id)).then(function(res){
			if (res.status == 200) {
				imageInfoCtrl.match_info = res.data;
			}
		});
	}

	imageInfoCtrl.image_upload = function(file){
		console.log('파일 업로드');
		Upload.upload({
			url: '/api/image/upload/' + parseInt($location.search().id),
			data: {image_file: file}
		}).then(function (resp) {
			if (resp.status == 200) {
				alert("파일전송성공");
				imageInfoCtrl.get_image_info();
			}else{
				alert("파일전송에 실패했습니다. 특수문자 등을 제거하고 다시 시도해 보시기 바랍니다.");
			}
		}, function (resp) {
			console.log('Error status: ' + resp.status);
		}, function (evt) {
			// var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			// console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
		});
	}

});