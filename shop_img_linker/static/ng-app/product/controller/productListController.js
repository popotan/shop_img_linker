angular.module('shop-img-linker')
.controller('productListController', function($scope, $element, $http, $location, $routeParams){
	var productListCtrl = this;

	productListCtrl.open_info = function(product_id){
		$location.search('id', product_id);
	}

	$scope.keyword = { prod_name : '' };

	productListCtrl.prod_info_init = {
		prod_name : ''
	};
	productListCtrl.add_product_list = function(){
		if (productListCtrl.prod_info_init.prod_name != '') {
			$http.post('/api/product', productListCtrl.prod_info_init).then(function(res){
				if (res.status == 200) {
					$scope.product_list.push(res.data);
					productListCtrl.open_info(res.data.prod_id);
					productListCtrl.prod_info_init.prod_name = '';
				}
			});
		}else{
			alert('상품명을 입력해주세요.');
		}
	}
});