angular.module('shop-img-linker')
.controller('productController', function($scope, $http, $location){
	var productCtrl = this;

	productCtrl.get_product_list = function(){
		$http.get('/api/product').then(function(res){
			if (res.status == 200) {
				$scope.product_list = res.data;
			}
		});
	}
	productCtrl.get_product_list();

	$scope.$on('product_list:remove', function(event, data){
		angular.forEach($scope.product_list, function(item, idx){
			if (item.prod_id == data.prod_id) {
				$scope.product_list.splice(idx, 1);
				$location.url($location.path());
			}
		});
	});

	$scope.$on('product_list:modify', function(event, data){
		angular.forEach($scope.product_list, function(item, idx){
			if (item.prod_id == data.prod_id) {
				$scope.product_list[idx] = data;
				return;
			}
		});
	});
});