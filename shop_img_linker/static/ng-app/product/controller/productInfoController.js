angular.module('shop-img-linker')
.controller('productInfoController', function($scope, $http, $location, $route){
	var productInfoCtrl = this;

	$scope.$watch(function(){
		return parseInt($location.search().id);
	}, function(newval, oldval){
		if (isNaN(newval)) return;
		// if (newval===oldval) return;
		productInfoCtrl.init();
		productInfoCtrl.get_prod_info();
	}, true);

	productInfoCtrl.image_preview_filter = function(list){
		result = [];
		for (var i = 0; i < list.length; i++) {
			if(list[i].shop.indexOf(productInfoCtrl.view_option) > -1){
				result.push(i);
			}
		}
		return result;
	}

	productInfoCtrl.init = function(){
		productInfoCtrl.prod_info = {};
		$scope.view_option = 'firstmall';
		$scope.match_info = [];
		productInfoCtrl.image_search_keyword = '';
		productInfoCtrl.image_search_list = [];
		productInfoCtrl.html_tag_result = '';
	}
	

	productInfoCtrl.get_prod_info = function(){
		if ($location.search().id > 0) {
			$http.get('/api/product/' + parseInt($location.search().id)).then(function(res){
				if (res.status == 200) {
					productInfoCtrl.prod_info = res.data;
					$scope.get_relative_image_list();
				}
			});
		}
	}

	productInfoCtrl.mod_prod_info = function(){
		$http.post('/api/product/' + parseInt($location.search().id), productInfoCtrl.prod_info).then(function(res){
			if (res.status == 200) {
				productInfoCtrl.prod_info = res.data;
				alert("변경내용이 반영되었습니다.");
				$scope.$emit('product_list:modify', res.data);
			}
		});
	}

	productInfoCtrl.remove_prod_info = function(){
		if (confirm('해당 상품정보를 삭제하시겠습니까?')) {
			$http.delete('/api/product/' + parseInt($location.search().id)).then(function(res){
				if (res.status == 201) {
					alert('삭제되었습니다.');
					productInfoCtrl.prod_info = undefined;
					$scope.$emit('product_list:remove', {prod_id : parseInt($location.search().id)});
				}
			});
		}
	}

	
	$scope.get_relative_image_list = function(){
		$http.get('/api/match/product/' + parseInt($location.search().id)).then(function(res){
			if (res.status == 200) {
				$scope.match_info = res.data;
			}
		});
	};

	$scope.$on('match_info:delete', function(event, data){
		angular.forEach($scope.match_info, function(item, idx){
			if (item.match_id == data.match_id) {
				$scope.match_info.splice(idx, 1);
				console.log($scope.match_info);
				return;
			}
		});
	});

	productInfoCtrl.search_image = function(){
		$http.get('/api/image/search', {params : {keyword : productInfoCtrl.image_search_keyword}})
		.then(function(res){
			productInfoCtrl.image_search_list = res.data;
		});
	}
	$scope.$watch(function(){
		return productInfoCtrl.image_search_keyword;
	}, function(newval, oldval){
		if (newval===oldval) return;
		if (newval == '') {
			productInfoCtrl.image_search_list = [];
		}else{
			productInfoCtrl.search_image();
		}
	});

	productInfoCtrl.match_image = function(image_id){
		$http.post('/api/match/image/with/' + parseInt($location.search().id), {image_id : image_id}).then(function(res){
			if (res.status == 200) {
				$scope.get_relative_image_list();
			}
		});
	}

	productInfoCtrl.get_html_tag = function(shop){
		$http.get('/api/style/' + parseInt($location.search().id) + '/' + shop).then(function(res){
			if (res.status == 200) {
				productInfoCtrl.html_tag_result = res.data;
			}
		});
	};

	productInfoCtrl.copy_html_tag = function(){
		function is_ie() {
			if(navigator.userAgent.toLowerCase().indexOf("chrome") != -1) return false;
			if(navigator.userAgent.toLowerCase().indexOf("msie") != -1) return true;
			if(navigator.userAgent.toLowerCase().indexOf("windows nt") != -1) return true;
			return false;
		}
		if( is_ie() ) {
			window.clipboardData.setData("Text", productInfoCtrl.html_tag_result);
			alert("복사되었습니다.");
			return;
		}
		prompt("Ctrl+C를 눌러 복사하세요.", productInfoCtrl.html_tag_result);
	}
})
.filter('view_by_shop', function() {
	return function(match_info, target) {
		filtered_list = [];
		angular.forEach(match_info, function(info){
			if(info.shop.indexOf(target) > -1){
				filtered_list.push(info);
			}
		});
		return filtered_list;
	};
});