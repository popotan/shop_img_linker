angular.module('shop-img-linker')
.directive('relativeImage', function($http){
	return {
		restrict : 'A',
		templateUrl : '/static/ng-app/product/element/relative-image.html',
		scope : {
			'product' : '=',
			'match' : '='
		},
		link : function(scope, element, attrs){

		},
		controller : function($scope, $element, $attrs){
			$scope.mod_match_info = function(){
				$http.post('/api/match/' + $scope.match.match_id, $scope.match).then(function(res){
					if(res.status == 200){
						$scope.match = res.data;
					}
				});
			}

			$scope.toggleSelection = function(target, value) {
				var idx = $scope.match[target].indexOf(value);
				// is currently selected
				if (idx > -1) {
					$scope.match[target].splice(idx, 1);
				}
				// is newly selected
				else {
					$scope.match[target].push(value);
				}
				$scope.mod_match_info();
			};

			$scope.switch_seq = function(direction){
				switch(direction){
					case 'up':
					if ($scope.$parent.$index == 0) {
						alert('최상단입니다.');
						return;
					}
					var pTarget = $scope.match.match_id;
					var iTarget = $scope.$parent.$parent.$parent.match_info[$scope.$parent.$index-1].match_id;
					break;
					case 'down':
					if ($scope.$parent.$index+1 >= $scope.$parent.$parent.$parent.match_info.length) {
						alert('최하단입니다.');
						return;
					}
					var pTarget = $scope.$parent.$parent.$parent.match_info[$scope.$parent.$index+1].match_id;
					var iTarget = $scope.match.match_id;
					break;
				}
				$http.post('/api/match/seq/switch/' + pTarget + '/to/' + iTarget).then(function(res){
					if (res.status == 200) {
						switch(direction){
							case 'up':
							$scope.$parent.$parent.$parent.match_info[$scope.$parent.$index] = res.data.iResult;
							$scope.$parent.$parent.$parent.match_info[$scope.$parent.$index-1] = res.data.pResult;
							break;
							case 'down':
							$scope.$parent.$parent.$parent.match_info[$scope.$parent.$index] = res.data.pResult;
							$scope.$parent.$parent.$parent.match_info[$scope.$parent.$index+1] = res.data.iResult;
							break;
						}
					}
				});
			}

			$scope.unlink = function(){
				if (confirm("해당 이미지를 목록에서 제거하시겠습니까?")) {
					$http.delete('/api/match/unlink/' + $scope.match.match_id).then(function(res){
						if (res.status == 201) {
							$scope.$emit('match_info:delete', {match_id : $scope.match.match_id});
							alert('삭제되었습니다.');
						}
					});
				}
			};

		}
	};
});